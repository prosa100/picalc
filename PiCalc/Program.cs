﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiCalc
{
    struct Vec2
    {
        public double X, Y;
        public double MagnitudeSqrd
        {
            get { return X * X + Y * Y; }
        }
        public Vec2(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
        public override string ToString()
        {
            return string.Format("({0},{1})", X, Y);
        }
    }
    struct Square
    {
        public double Size;
        public Vec2 Midpoint;

        public Square TopLeftQuadrant
        {
            get
            {
                return new Square
                {
                    Size = Size / 2,
                    Midpoint = new Vec2(Midpoint.X + Size / 2, Midpoint.Y + Size / 2)
                };
            }
        }

        public Square TopRightQuadrant
        {
            get
            {
                return new Square
                {
                    Size = Size / 2,
                    Midpoint = new Vec2(Midpoint.X - Size / 2, Midpoint.Y + Size / 2)
                };
            }
        }

        public Square BottomLeftQuadrant
        {
            get
            {
                return new Square
                {
                    Size = Size / 2,
                    Midpoint = new Vec2(Midpoint.X + Size / 2, Midpoint.Y - Size / 2)
                };
            }
        }

        public Square BottomRightQuadrant
        {
            get
            {
                return new Square
                {
                    Size = Size / 2,
                    Midpoint = new Vec2(Midpoint.X - Size / 2, Midpoint.Y - Size / 2)
                };
            }
        }

        public Square[] Quadrants { get { return new[] { BottomRightQuadrant, BottomLeftQuadrant, TopLeftQuadrant, TopRightQuadrant }; } }

        public override string ToString()
        {
            return string.Format("({0},{1})", Midpoint, Size);
        }
    }
    class Program
    {
        public static double T = 1d / (2<<10);

        static void Main(string[] args)
        {
            var pi = 4 * F(new Square
            {
                Size = 1,
                Midpoint = new Vec2(0, 0)
            });
            Console.WriteLine("Pi={0}", pi);
            Console.WriteLine("Error = {0:P5}", Math.Abs(pi - Math.PI) / Math.PI);
            Console.ReadLine();
        }

        public static double F(Square r)
        {
            return (r.Size < T)?(r.Midpoint.MagnitudeSqrd < 1 ? 1 : 0): r.Quadrants.Select(F).Average();
        }
    }
}
